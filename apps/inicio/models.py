from django.db import models
from django.utils import timezone

# Create your models here.
# - TipoInmueble (Casa, Departamento)
# - TipoAnuncio (Venta, Compra)

class TipoInmueble(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=50, null=False, blank=False)

    class Meta:
        verbose_name = "Tipo inmueble"
        verbose_name_plural = "Tipos inmuebles"

    def __str__(self):
        return self.nombre

class TipoAnuncio(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=50, null=False, blank=False)
    class Meta:
        verbose_name = "Tipo anuncio"
        verbose_name_plural = "Tipos anuncios"

    def __str__(self):
        return self.nombre

class Region(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=50, null=False, blank=False)

    class Meta:
        verbose_name = "Región"
        verbose_name_plural = "Regiones"

    def __str__(self):
        return self.nombre

class Comuna(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=50, null=False, blank=False)
    id_region = models.ForeignKey(Region, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Comuna"
        verbose_name_plural = "Comunas"

    def __str__(self):
        return self.nombre

class Direccion(models.Model):
    id = models.AutoField(primary_key = True)
    id_region = models.ForeignKey(Region, on_delete=models.CASCADE)
    id_comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)
    calle = models.CharField(max_length=150, null=False, blank=False)
    numero = models.CharField(max_length=7, null=False, blank=False)
    detalle = models.CharField(max_length=255, null=True, blank=True)
    codigo_postal = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = "Direccion"
        verbose_name_plural = "Direcciones"

    def __str__(self):
        return self.calle + ' ' + self.numero

class Galeria(models.Model):
    id = models.AutoField(primary_key = True)
    descripcion = models.CharField(max_length=100, null=False, blank=False)
    contenido = models.ImageField(verbose_name="Imagen", upload_to="anuncios")

    class Meta:
        verbose_name = "Galeria"
        verbose_name_plural = "Galerias"

    def __str__(self):
        return self.descripcion

class Anuncio(models.Model):
    id = models.AutoField(primary_key = True)
    id_tipo_anuncio = models.ForeignKey(TipoAnuncio, null=True, blank=True, on_delete=models.DO_NOTHING)
    id_tipo_inmueble = models.ForeignKey(TipoInmueble, null=True, blank=True, on_delete=models.DO_NOTHING)
    id_direccion = models.ForeignKey(Direccion, null=True, blank=True, on_delete=models.DO_NOTHING)
    id_galeria = models.ForeignKey(Galeria, null=True, blank=True, on_delete=models.CASCADE)
    precio = models.IntegerField(null=False, blank=False)
    metros_cuadrados = models.IntegerField(null=False, blank=False)
    cantidad_dormitorios = models.IntegerField(null=False, blank=False)
    cantidad_banios = models.IntegerField(null=False, blank=False)
    descripcion = models.CharField(max_length=500, null=False, blank=False)
    imagen = models.ImageField(verbose_name="Imagen", upload_to="anuncios", null=True, blank=True)
    fecha_publicacion = models.DateField(default=timezone.now)
    fecha_modificacion = models.DateField(default=timezone.now)

    class Meta:
        verbose_name = "Anuncio"
        verbose_name_plural = "Anuncios"

    def __str__(self):
        return self.descripcion

class Cliente(models.Model):
    id = models.AutoField(primary_key = True)
    nombre = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    email = models.EmailField(max_length=254)
    movil = models.IntegerField()
    fijo = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    def __str__(self):
        return self.nombre

class Cotizacion(models.Model):
    id = models.AutoField(primary_key = True)
    id_cliente = models.ForeignKey(Cliente, on_delete=models.DO_NOTHING)
    fecha_cotizacion = models.DateField(default=timezone.now)

    class Meta:
        verbose_name = "Cotización"
        verbose_name_plural = "Cotizaciones"
