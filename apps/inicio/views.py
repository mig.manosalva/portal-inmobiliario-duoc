from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from .forms import AnuncioForm
from .models import Anuncio, Direccion, Galeria, TipoAnuncio, TipoInmueble, Region, Comuna, Direccion

# Create your views here.
def Home(request):
    tiposAnuncios = TipoAnuncio.objects.all()
    tiposInmuebles = TipoInmueble.objects.all()
    anuncios = Anuncio.objects.all()
    regiones = Region.objects.all()
    comunas =  Comuna.objects.all()
    contexto = {'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }
    if request.method == 'POST':
        descripcion_    = request.POST.get('busqueda')
        tipoInmueble_   = request.POST.get('tipo_inmueble')
        tipoAnuncio_    = request.POST.get('tipo_anuncio')
        anuncios = Anuncio.objects.filter(id_tipo_anuncio=tipoAnuncio_, id_tipo_inmueble=tipoInmueble_)
        contexto = {'anuncios': anuncios, 'regiones': regiones, 'comunas': comunas, 'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }
        return ListarAnuncios(request, tipoInmueble_, tipoAnuncio_, descripcion_)
        #return ListarAnuncios(request)
    return render(request, 'inicio/inicio.html', contexto)

##### A N U N C I O S
def ListarAnuncios(request, tipoInmueble_R = None, tipoAnuncio_R = None, descripcion_R = None):
    anuncios = Anuncio.objects.filter(id_tipo_anuncio=tipoAnuncio_R, id_tipo_inmueble=tipoInmueble_R)
    regiones = Region.objects.all()
    comunas =  Comuna.objects.all()
    tiposAnuncios = TipoAnuncio.objects.all()
    tiposInmuebles = TipoInmueble.objects.all()
    contexto = {'anuncios': anuncios, 'regiones': regiones, 'comunas': comunas, 'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }


    if request.method == 'POST':
        if request.POST.get('busqueda'):
            numeroHabitaciones_ = request.POST.get('numero_habitaciones')
            numeroBanios_       = request.POST.get('numero_banios')
            region_             = request.POST.get('region')
            comuna_             = request.POST.get('comuna')
            tipoInmueble_       = request.POST.get('tipo_inmueble')
            tipoAnuncio_        = request.POST.get('tipo_anuncio')
            anuncios = Anuncio.objects.filter(id_tipo_anuncio=tipoAnuncio_, id_tipo_inmueble=tipoInmueble_, cantidad_dormitorios=numeroHabitaciones_, cantidad_banios=numeroBanios_ )
            contexto = {'anuncios': anuncios, 'regiones': regiones, 'comunas': comunas, 'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }
            return render(request, 'anuncio/anuncios.html', contexto)

        if request.POST.get('todos'):
            print("#Todos", tipoInmueble_R)
            anuncios = Anuncio.objects.all()
            contexto = {'anuncios': anuncios, 'regiones': regiones, 'comunas': comunas, 'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }
            return render(request, 'anuncio/anuncios.html', contexto)

    return render(request, 'anuncio/anuncios.html', contexto)

def DetalleAnuncio(request, id):
    anuncio = Anuncio.objects.get(id = id)

    return render(request, 'anuncio/anuncio.html', {'anuncio': anuncio})

def CrearAnuncio(request):
    regiones = Region.objects.all()
    comunas =  Comuna.objects.all()
    tiposAnuncios = TipoAnuncio.objects.all()
    tiposInmuebles = TipoInmueble.objects.all()
    galeria = Galeria.objects.all()

    contexto = {'galeria': galeria, 'regiones': regiones, 'comunas': comunas, 'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }

    if request.method == 'POST':
        #anuncio_form = AnuncioForm(request.POST)
        tipoAnuncio         = request.POST.get('tipo_anuncio')
        tipoInmueble        = request.POST.get('tipo_inmueble')
        precio              = request.POST.get('precio')
        metrosCuadrados     = request.POST.get('metros_cuadrados')
        cantidadDormitorios = request.POST.get('cantidad_dormitorios')
        cantidadBanios      = request.POST.get('cantidad_banios')
        region              = request.POST.get('region')
        comuna              = request.POST.get('comuna')
        calle               = request.POST.get('calle')
        numero              = request.POST.get('numero')
        detalle             = request.POST.get('detalle')
        imagen              = request.FILES.get('imagen')
        descripcion         = request.POST.get('descripcion')

        # - Generar dirección para guardarla en el anuncio
        # Nota: se establece dirección en un objeto para posteriormente usarla como direcciones del usuario,
        #       se debe modificar el modelo para que las direcciones y anuncios queden asociacdas a un usuario
        nuevaDireccion = Direccion(
            id_region = Region.objects.get(id=region),
            id_comuna = Comuna.objects.get(id=comuna),
            detalle = detalle,
            calle = calle,
            numero = numero
        )
        nuevaDireccion.save()
        ultimaDireccion = Direccion.objects.last()

        nuevoAnuncio = Anuncio(
            id_tipo_anuncio = TipoAnuncio.objects.get(id=tipoAnuncio),
            id_tipo_inmueble = TipoInmueble.objects.get(id=tipoInmueble),
            id_direccion = Direccion.objects.get(id=ultimaDireccion.id),
            precio = precio,
            imagen = imagen,
            metros_cuadrados = metrosCuadrados,
            cantidad_dormitorios = cantidadDormitorios,
            cantidad_banios = cantidadBanios,
            descripcion = descripcion,
        )

        nuevoAnuncio.save()
        return redirect('/anuncios/')

    return render(request, 'anuncio/crear_anuncio.html', contexto)

def EditarAnuncio(request, id):
    anuncio = Anuncio.objects.get(id = id)

    regiones = Region.objects.all()
    comunas =  Comuna.objects.all()
    tiposAnuncios = TipoAnuncio.objects.all()
    tiposInmuebles = TipoInmueble.objects.all()
    galeria = Galeria.objects.all()
    contexto = {'galeria': galeria, 'regiones': regiones, 'comunas': comunas, 'tiposAnuncios': tiposAnuncios, 'tiposInmuebles': tiposInmuebles }
    calle = request.GET.get('calle')
    error = None
    try:

        if request.method == 'POST':
            #anuncio_form = AnuncioForm(request.POST, instance = anuncio)
            #anuncio_form = AnuncioForm(request.POST)
            tipoAnuncio         = request.POST.post('tipo_anuncio')
            tipoInmueble        = request.POST.get('tipo_inmueble')
            precio              = request.POST.get('precio')
            metrosCuadrados     = request.POST.get('metros_cuadrados')
            cantidadDormitorios = request.POST.get('cantidad_dormitorios')
            cantidadBanios      = request.POST.get('cantidad_banios')
            region              = request.POST.get('region')
            comuna              = request.POST.get('comuna')
            calle               = request.POST.post('calle')
            numero              = request.POST.get('numero')
            detalle             = request.POST.get('detalle')
            imagen              = request.FILES.get('imagen')
            descripcion         = request.POST.get('descripcion')

            # - Generar dirección para guardarla en el anuncio
            # Nota: se establece dirección en un objeto para posteriormente usarla como direcciones del usuario,
            #       se debe modificar el modelo para que las direcciones y anuncios queden asociacdas a un usuario
            nuevaDireccion = Direccion(
                id_region = Region.objects.get(id=region),
                id_comuna = Comuna.objects.get(id=comuna),
                detalle = detalle,
                calle = calle,
                numero = numero
            )
            nuevaDireccion.save()
            ultimaDireccion = Direccion.objects.last()

            nuevoAnuncio = Anuncio(
                id_tipo_anuncio = TipoAnuncio.objects.get(id=tipoAnuncio),
                id_tipo_inmueble = TipoInmueble.objects.get(id=tipoInmueble),
                id_direccion = Direccion.objects.get(id=ultimaDireccion.id),
                precio = precio,
                imagen = imagen,
                metros_cuadrados = metrosCuadrados,
                cantidad_dormitorios = cantidadDormitorios,
                cantidad_banios = cantidadBanios,
                descripcion = descripcion,
            )

            nuevoAnuncio.save()
            return redirect('/anuncios/')

    except ObjectDoesNotExist as e:
        error = e
    return render(request, 'anuncio/crear_anuncio.html', {'error': error})

def EliminarAnuncio(request, id):
    anuncio = Anuncio.objects.get(id = id)
    anuncio.delete()
    return redirect('/anuncios/')
