from django import forms
from .models import Anuncio

class AnuncioForm(forms.ModelForm):
    class Meta:
        model = Anuncio
        fields = ['id_tipo_anuncio', 'id_tipo_inmueble', 'precio', 'metros_cuadrados', 'cantidad_dormitorios', 'cantidad_banios', 'descripcion']
