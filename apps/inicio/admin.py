from django.contrib import admin
from .models import Region,Comuna,Direccion,TipoInmueble, TipoAnuncio, Galeria, Cliente,Anuncio,Direccion

# Register your models here.

admin.site.register(Region)
admin.site.register(Comuna)
admin.site.register(Direccion)
admin.site.register(TipoInmueble)
admin.site.register(TipoAnuncio)
admin.site.register(Galeria)
admin.site.register(Cliente)
admin.site.register(Anuncio)
