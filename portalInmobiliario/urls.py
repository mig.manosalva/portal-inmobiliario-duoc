"""portalInmobiliario URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from apps.inicio.views import Home, CrearAnuncio, ListarAnuncios, DetalleAnuncio, EditarAnuncio, EliminarAnuncio

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Home, name = 'Inicio' ),
    path('crear_anuncio/', CrearAnuncio, name='crear_anuncio'),
    path('anuncios/', ListarAnuncios, name='anuncios'),
    path('anuncio/<int:id>', DetalleAnuncio, name='detalle_anuncio'),
    #path('anuncios/', ListarAnuncios, name='anuncios'),
    #path('anuncios/<slug:busqueda>', ListarAnuncios, name='anuncios'),
    path('editar_anuncio/<int:id>', EditarAnuncio, name='editar_anuncio'),
    path('eliminar_anuncio/<int:id>', EliminarAnuncio, name='eliminar_anuncio'),
]
