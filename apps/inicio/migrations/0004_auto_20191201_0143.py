# Generated by Django 2.2.7 on 2019-12-01 04:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('inicio', '0003_auto_20191201_0137'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='direccion',
            options={'verbose_name': 'Dirección', 'verbose_name_plural': 'Direcciones'},
        ),
    ]
